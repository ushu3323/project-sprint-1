// Metodo de pago
class PaymentMethod {
    constructor(id, name) {
        this.id = id;
        this.name = name;
    }
}

module.exports = PaymentMethod;